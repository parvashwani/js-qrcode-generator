var qr;

(function(){
    qr = new QRious({
        element: document.getElementById('qr-code'),
        size: 200,
        value: "https://thegeekycoder.xyz/"
    });
})();

function generateQRCode() {
    var qrtext = document.getElementById("qr-text").value;
    document.getElementById("qr-result").innerHTML = "QR Code For " + qrtext + ":";
    qr.set({
        foreground: "black",
        size: 200,
        value: qrtext
    });
}